package Cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.jira_xray.XRayCucumberFeatureFetcher;
import utils.jira_xray.XRayCucumberResultsImporter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyJiraStepdefs {
    List<String> ids = new ArrayList<>();
    File outputDir;

    @Given("feature ids are defined")
    public void featureIdsAreDefined() {
        ids.add("TAAADEV-747");
//        ids.add("TAAADEV-759");
        ids.add("TAAADEV-811");
        outputDir = new File("src/test/java/Cucumber/Resources/JiraFeatures");
    }

    @When("request to Jira XRay is sent")
    public void requestToJiraXRayIsSent() {
        XRayCucumberFeatureFetcher xRayCucumberFeatureFetcher = new XRayCucumberFeatureFetcher();
        xRayCucumberFeatureFetcher.fetch(ids, outputDir);
    }

    @Then("feature files are received")
    public void featureFilesAreReceived() {
        //check if the folder is not empty
        //$$("src/test/java/Cucumber/Resources/JiraFeatures").isEmpty();
    }

    @Given("file with results is present")
    public void fileWithResultsIsPresent() {
    }

    @When("results to Jira XRay are sent")
    public void resultsToJiraXRayAreSent() {
        XRayCucumberResultsImporter xRayCucumberResultsImporter = new XRayCucumberResultsImporter();
        xRayCucumberResultsImporter.importResults(new File("target/cucumber-html-reports/cucumber.json"));
    }

    @Then("results are added")
    public void resultsAreAdded() {
        //let it be empty
    }


}
