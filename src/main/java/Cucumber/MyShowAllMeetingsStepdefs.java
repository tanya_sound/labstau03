package Cucumber;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import static com.codeborne.selenide.Selenide.screenshot;

public class MyShowAllMeetingsStepdefs {
    @When("Click on Startseite")
    public void clickOnStartseite() {
        $(By.id("dashboard_nav_home_item")).shouldBe(visible).shouldBe(enabled).click();
    }

    @And("Click on View all meetings")
    public void ClickViewAllMeetings() {
        //m_lists_viewall
        $("a.m_lists_viewall").shouldBe(enabled).click();
    }

    @And("Select the whole month")
    public void selectWholeMonth() {
        //Нажимаем на поле для выбора дат
        $("input#datePickerID.el-input__inner").shouldBe(enabled).click();
        //screenshot("ChooseDateScreen");

        //Кликаем на 01.10.2021
        $(By.xpath("//td[@aria-label='Friday 1 October, 2021']")).click();
        screenshot("ChooseStartDate");

        //Кликаем на 31.10.2021
        // заменили день недели с @aria-label='Sunday 31 October, 2021' на @aria-label='Saturday 31 October, 2021'
        //тогда тест упадет, т.к. не будет найден элемент - конечная дата и в отчет попадет скриншот
        $(By.xpath("//td[@aria-label='Sunday 31 October, 2021']")).click();

        //Кликаем на OK
        $("button.el-button.el-button--primary.el-time-panel__btn.el-button--button").shouldBe(enabled).click();
    }

    @Then("Correct meeting list is shown")
    public void correctMeetingListShown() {
        //Check that there are ten meetings list on the page
        //Если поставить количество ожидаемы митингов, например, 10, то тест тоже упадет
        //и в отчет попадет скриншот
        $$(By.xpath("//li[contains(@class, 'm_list_item')]")).shouldHave(CollectionCondition.size(9));
        List<SelenideElement> monthMeetingNumber = new ArrayList<>();
        ElementsCollection monthMeetingNumberCollection = $$(By.xpath("//li[contains(@class, 'm_list_item')]"));
        for (SelenideElement curMeeting : monthMeetingNumberCollection) {
            if (curMeeting.$(".meeting_topic").getText().contains("Test Automation")) {
                monthMeetingNumber.add(curMeeting);
            }
        }
        //System.out.println(monthMeetingNumber.size());
        for (SelenideElement curMeeting : monthMeetingNumber) {
            curMeeting.$("[title][class=list_t]").exists();
            curMeeting.$("[title][class=list_t]").shouldHave(Condition.text("16:00 - 19:00"));
        }
    }

}
