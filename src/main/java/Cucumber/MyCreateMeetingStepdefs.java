package Cucumber;

import com.codeborne.selenide.Condition;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class MyCreateMeetingStepdefs {
//    @Given("user is logged in")
//    public void userIsLoggedIn() {
//        Configuration.startMaximized = true;
//        Configuration.holdBrowserOpen = true;
//
//        open("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
//        $("title").shouldHave(attribute("text", "Cisco Webex Meetings - Home"));
//
//        $(By.id("guest_signin_split_button-action")).shouldBe(Condition.visible).click();
//        $("input#IDToken1").val(PropertyLoader.loadProperty("email"));
//        $(By.id("IDButton2")).shouldBe(Condition.enabled).click();
//        $(By.xpath("//span[contains(text(), 'MyPortal Account (Alternative)')]")).should(Condition.visible).click();
//
//        $(By.id("username")).val(PropertyLoader.loadProperty("email"));
//
//        $(By.id("password")).val(PropertyLoader.loadProperty("emailPassword"));
//
//        $(By.id("submit")).click();
//
//        //Check that there are four meetings list on the page
//        $$(By.xpath("//li[contains(@class, 'm_list_item')]")).shouldHave(CollectionCondition.size(4));
//    }

    @When("go to meetings")
    public void goToMeetings() {
        //Нажимаем на Meetings в левой части экрана
//        $(By.id("dashboard_nav_meeting_item")).shouldBe(Condition.enabled).click();
        $("a#dashboard_nav_meeting_item").shouldBe(Condition.enabled).click();
    }

    @And("Click on Ein Meeting ansetzen")
    public void clickOnEinMeetingAnsetzen() {
        $(By.id("scheduleEntry")).shouldBe(Condition.enabled).click();
    }

    @And("Enter Thema des Meetings")
    public void enterThemaDesMeetings() {
//        $(By.id("topic")).val(Keys.CONTROL + "A");
//        $(By.id("topic")).val(String.valueOf(Keys.BACK_SPACE));
//        $(By.id("topic")).val("Test Meeting");

        $("input#topic.el-input__inner").val(Keys.CONTROL + "A");
        $("input#topic.el-input__inner").val(String.valueOf(Keys.BACK_SPACE));
        $("input#topic.el-input__inner").val("Test Meeting");
    }

    @And("Enter email addresses into Teilnehmer")
    public void enterEmailAddressesIntoTeilnehmer() {
        //invite_attendee_input
        $(By.id("invite_attendee_input")).val("tatiana.kozlova@t-systems.com");
    }

    @And("Change date and timebox")
    public void changeDateAndTimebox() {
        //нажимаем на стрелку вниз (scheduleTimeTrigger) для выбора даты и времени митинга
        $(By.id("scheduleTimeTrigger")).shouldBe(Condition.enabled).click();

        //$(By.xpath("//td[@aria-label='Thursday 30 September, 2021']")).click();

        //Нажимаем на стрелку вправо для смены месяца на следующий
        //(button.el-picker-panel__icon-btn.el-date-picker__next-btn.icon-ng-right)
        $("button.el-picker-panel__icon-btn.el-date-picker__next-btn.icon-ng-right").shouldBe(Condition.enabled).click();

        //Выбираем дату - 30.10.2021
        //$(By.xpath("//td[@aria-label='Saturday 30 October, 2021']")).click();

        //Tuesday 30 November, 2021
        //Выбираем дату - 30.11.2021
        $(By.xpath("//td[@aria-label='Tuesday 30 November, 2021']")).click();

        //Нажимаем Abgeschlossen/Done
//        $(By.id("scheduleDone")).shouldBe(Condition.enabled).click();

    }

    @And("Save a new meeting")
    public void saveNewMeeting() {
        //Нажимаем на кнопку Termin ansetzen/Start (id = scheduleEdit)
        $(By.id("scheduleEdit")).shouldBe(Condition.enabled).click();
    }

    @Then("A new meeting is successfully created")
    public void newMeetingIsCreated() {
        //Проверяем, что митинг успешно создан. Должны быть выполнены следующие условия:

        //1. Открылась страничка, на которой в заголовке название митинга - Test Meeting
        //meeting_topic_wrapper
        $(By.xpath("//h1[@title='Test Meeting']")).shouldHave(Condition.text("Test Meeting"));
//        $("h1").shouldHave(Condition.text("Test Meeting"));

        //2. в списке участнииков есть участник Tatiana Kozlova
//        $(By.xpath("//div[@title='tatiana.kozlova@t-systems.com']")).shouldHave(Condition.text("Tatiana Kozlova"));
        $(By.xpath("//div[@class='mdp_attendee_name']")).shouldHave(Condition.text("Tatiana Kozlova"));
        $("div.mdp_attendee_name").shouldHave(Condition.text("Tatiana Kozlova"));

        //3. Кнопка Start Meeting/Meeting beginnen присутствует
        $(By.id("smartJoinButton-trigger")).shouldBe(visible).shouldBe(enabled);
    }
}
