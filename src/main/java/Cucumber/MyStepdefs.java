package Cucumber;

import Pages.LoginPage;
import Pages.PreferencesPage;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Allure;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import utils.PropertyLoader;

import javax.swing.*;
import java.io.ByteArrayInputStream;

import static com.codeborne.selenide.Selenide.*;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.*;

public class MyStepdefs {
    @After
    public void onTestFailure(Scenario scenario) {
        if (scenario.isFailed()) {
            Allure.addAttachment("Any text", new ByteArrayInputStream(((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES)));
        }
    }

    LoginPage loginPage;
    PreferencesPage preferencesPage;

    @Given("user is logged in")
    public void userIsLoggedIn() {
        Configuration.startMaximized = true;
        Configuration.holdBrowserOpen = true;
        Configuration.reportsFolder = "C:\\Users\\tkozlova\\IdeaProjects\\LabsTAU03\\target\\ScreenShots";

        loginPage = open("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag", LoginPage.class);
        loginPage.clickOnSignInBtn();
        loginPage.setEmail(PropertyLoader.loadProperty("email"));
        loginPage.clickBntOkOnStartPage();
        loginPage.clickOnMyPortalBtn();
        loginPage.myPortalInsertEmail(PropertyLoader.loadProperty("email"));
        loginPage.myPortalInsertPassword(PropertyLoader.loadProperty("emailPassword"));
        loginPage.clickOnMyPortalSubmitBtn();
        loginPage.pageLoadedCheck();

//        open("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
//        $("title").shouldHave(attribute("text", "Cisco Webex Meetings - Home"));
//
//        $(By.id("guest_signin_split_button-action")).shouldBe(Condition.visible).click();
//        $("input#IDToken1").val(PropertyLoader.loadProperty("email"));
//        $(By.id("IDButton2")).shouldBe(Condition.enabled).click();
//        $(By.xpath("//span[contains(text(), 'MyPortal Account (Alternative)')]")).should(Condition.visible).click();
//
//        $(By.id("username")).val(PropertyLoader.loadProperty("email"));
//
//        $(By.id("password")).val(PropertyLoader.loadProperty("emailPassword"));
//
//        $(By.id("submit")).click();
//
//        //Check that there are four meetings list on the page
//        $$(By.xpath("//li[contains(@class, 'm_list_item')]")).shouldHave(CollectionCondition.size(4));
//
//        //Check that the loaded room name is Persönlicher Raum von Tatiana Kozlova
//        $(By.xpath("//h2[contains(text(), 'Tatiana Kozlova')]")).shouldHave(Condition.text("Persönlicher Raum von Tatiana Kozlova"));
    }

    @When("go to preferences")
    public void goToPreferences() {
        preferencesPage = open("https://dtag.webex.com/webappng/sites/dtag/preference/home", PreferencesPage.class);
        preferencesPage.clickOnPreferencesBtn();

//        $(By.id("dashboard_nav_preference_item")).shouldBe(Condition.enabled).click();
    }

    @And("open tab My room")
    public void openTabMyRoom() {
        preferencesPage.clickMyPersonalRoomTab();

//        $(By.id("preference_myPersonalRoom_tab")).shouldBe(Condition.enabled).click();
    }

    @And("change and save My room name")
    public void changeAndSaveMyRoomName() {
        preferencesPage.setPersonalRoomNameFld("Persönlicher Raum von Tatiana Kozlova*");
        preferencesPage.clickPersonalRoomSaveBtn();

//        $(By.id("prefPmrName")).val(Keys.CONTROL + "A" + " ");
//        $(By.id("prefPmrName")).val("Persönlicher Raum von Tatiana Kozlova*");
//
//        $(By.id("preference_myPersonalRoom_save_button")).shouldBe(Condition.enabled).click();
    }

    @Then("successful message is received")
    public void successfulMessageIsReceived() {
        preferencesPage.myPersonalRoomLoadedCheck();

//        $(By.xpath("//div[@class = 'el-message__group']//p")).shouldHave(Condition.text("Ihre Änderungen wurden gespeichert."));
    }
}
