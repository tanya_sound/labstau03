package Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;

public class PreferencesPage {
    @FindBy(id = "dashboard_nav_preference_item")
    private SelenideElement preferencesBtn;
    public void clickOnPreferencesBtn(){
        preferencesBtn.shouldBe(Condition.enabled).click();
    }

    @FindBy(id = "preference_myPersonalRoom_tab")
    private SelenideElement myPersonalRoomTab;
    public void clickMyPersonalRoomTab(){
        myPersonalRoomTab.shouldBe(Condition.enabled).click();
    }

    @FindBy(id = "prefPmrName")
    private SelenideElement personalRoomNameFld;
    public void setPersonalRoomNameFld(String roomName){
        personalRoomNameFld.val(Keys.CONTROL + "A" + " ");
        personalRoomNameFld.val(roomName);
    }

    @FindBy(id = "preference_myPersonalRoom_save_button")
    private SelenideElement personalRoomSaveBtn;
    public void clickPersonalRoomSaveBtn(){
        personalRoomSaveBtn.shouldBe(Condition.enabled).click();
    }

//        $(By.xpath("//div[@class = 'el-message__group']//p")).shouldHave(Condition.text("Ihre Änderungen wurden gespeichert."));
    @FindBy(xpath = "//div[@class = 'el-message__group']//p")
    private SelenideElement myPersonalRoomLoaded;
    public void myPersonalRoomLoadedCheck(){
        myPersonalRoomLoaded.shouldHave(Condition.text("Ihre Änderungen wurden gespeichert."));
    }

}
