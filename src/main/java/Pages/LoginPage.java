package Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
    @FindBy(id = "guest_signin_split_button-action")
    private SelenideElement signInBtn;
    public void clickOnSignInBtn() {
        signInBtn.shouldBe(Condition.visible).click();
    }

    @FindBy(id = "IDToken1")
    private SelenideElement emailInputField;
    public void setEmail(String email) {
        emailInputField.val(email);
    }

    @FindBy(name = "btnOK")
    private SelenideElement btnOK;
    public void clickBntOkOnStartPage() {
        btnOK.shouldBe(Condition.enabled).click();
    }

    @FindBy(xpath = "//span[contains(text(), 'MyPortal Account (Alternative)')]")
    private SelenideElement myPortalBtn;
    public void clickOnMyPortalBtn() {
        myPortalBtn.should(Condition.visible).click();
    }

    @FindBy(id = "username")
    private SelenideElement myPortalEmailInputField;
    public void myPortalInsertEmail(String email) {
        myPortalEmailInputField.val(email);
    }

    @FindBy(id = "password")
    private SelenideElement myPortalPasswordInputField;
    public void myPortalInsertPassword(String password) {
        myPortalPasswordInputField.val(password);
    }

    @FindBy(id = "submit")
    private SelenideElement myPortalSubmitBtn;
    public void clickOnMyPortalSubmitBtn() {
        myPortalSubmitBtn.click();
    }

    //check that necessary page is loaded
    @FindBy(xpath = "//h2[contains(text(), 'Tatiana Kozlova')]")
    private SelenideElement pageLoaded;
    public void pageLoadedCheck(){
        pageLoaded.shouldHave(Condition.text("Persönlicher Raum von Tatiana Kozlova"));
    }
}
