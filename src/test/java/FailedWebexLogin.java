import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class FailedWebexLogin {
    @Test
    public void loginWebex() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");

        Assert.assertEquals(webDriver.getTitle(), "Cisco Webex Meetings - Home", "Cisco Webex Meetings - Home page is not opened");

        WebElement signInButton = webDriver.findElement(By.id("guest_signin_split_button-action"));
        Assert.assertTrue(signInButton.isDisplayed(), "SignIn button is not displayed");
        signInButton.click();

        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(3, TimeUnit.SECONDS);

//        Assert.assertTrue(signInButton.isDisplayed(), "SignIn button is not displayed");
//        signInButton.isDisplayed();

        WebElement EmailInputField = webDriver.findElement(By.cssSelector("input#IDToken1"));
//        WebElement EmailInputField = webDriver.findElement(By.id("IDToken1"));
        EmailInputField.sendKeys("Tatiana.Kozlova123#t-systems.com");
        WebElement EmailInputFieldFocusLost = webDriver.findElement(By.xpath("//div[contains(text(), 'Welcome to Webex')]"));
        EmailInputFieldFocusLost.click();

        WebElement errorMessageText = webDriver.findElement(By.id("nameContextualError1"));
        Assert.assertTrue(true, "Enter a valid email address. Example: name@email.com");

        webDriver.quit();

    }
}
