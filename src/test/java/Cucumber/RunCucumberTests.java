package Cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/Cucumber/Resources/JiraFeatures/1.feature"},
        plugin = {"pretty", "json:target/cucumber-html-reports/cucumber.json", "io.qameta.allure.cucumber6jvm.AllureCucumber6Jvm"},
        publish = true
)

public class RunCucumberTests {
}
