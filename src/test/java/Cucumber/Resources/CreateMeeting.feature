Feature: Create Webex meeting
  Scenario: Create a new meeting
    Given user is logged in
    When go to meetings
    And Click on Ein Meeting ansetzen
    And Enter Thema des Meetings
    And Enter email addresses into Teilnehmer
    And Change date and timebox
    And Save a new meeting
    Then A new meeting is successfully created