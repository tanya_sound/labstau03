Feature: Change preferences
  Scenario: Change my room name
    Given  user is logged in
    When  go to preferences
    And  open tab My room
    And  change and save My room name
    Then  successful message is received


