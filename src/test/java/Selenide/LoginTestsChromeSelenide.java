package Selenide;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.PropertyLoader;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class LoginTestsChromeSelenide {
    @BeforeMethod
    public void fisrtPageInit() {
        Configuration.startMaximized = true;
        Configuration.holdBrowserOpen = true;

        open("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        $("title").shouldHave(attribute("text", "Cisco Webex Meetings - Home"));

        $(By.id("guest_signin_split_button-action")).shouldBe(Condition.visible).click();
    }

    @Test
    public void loginWebex() {
//        $(By.id("IDToken1")).val(PropertyLoader.loadProperty("email"));
        $("input#IDToken1").val(PropertyLoader.loadProperty("email"));
//        $("input#IDToken1").val("Tatiana.Kozlova@t-systems.com");
//        $("input#IDToken1").pressTab();

        $(By.id("IDButton2")).shouldBe(Condition.enabled).click();
//        $(By.name("btnOk")).shouldBe(Condition.enabled).click();

        $(By.xpath("//span[contains(text(), 'MyPortal Account (Alternative)')]")).should(Condition.visible).click();

        $(By.id("username")).val(PropertyLoader.loadProperty("email"));
//        $(By.id("username")).val("tkozlova");

        $(By.id("password")).val(PropertyLoader.loadProperty("emailPassword"));
//        $(By.id("password")).val("Tanya202106*");

        $(By.id("submit")).click();

        //Check that there are four meetings list on the page
        $$(By.xpath("//li[contains(@class, 'm_list_item')]")).shouldHave(CollectionCondition.size(4));

        //Check that the loaded room name is Persönlicher Raum von Tatiana Kozlova
        $(By.xpath("//h2[contains(text(), 'Tatiana Kozlova')]")).shouldHave(text("Persönlicher Raum von Tatiana Kozlova"));
//        $(By.xpath("//h2[contains(text(), 'Tatiana Kozlova')]")).shouldBe(Condition.matchText("Persönlicher Raum von Tatiana Kozlova"));
    }
}
