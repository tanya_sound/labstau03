package Selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseClass {
    public WebDriver webDriver;

    @BeforeClass
    public void init() {
        webDriver = new ChromeDriver();
        webDriver.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");

        webDriver.manage().window().maximize();

        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
    }

    @AfterClass
    public void tearDown() {
        //       webDriver.quit();
    }


}
