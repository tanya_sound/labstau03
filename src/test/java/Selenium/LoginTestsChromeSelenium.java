package Selenium;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;


public class LoginTestsChromeSelenium extends BaseClass {
    @BeforeMethod
    public void fisrtPageInit() {

        //Check that Cisco Webex Meetings - Home is loaded
        Assert.assertEquals(webDriver.getTitle(), "Cisco Webex Meetings - Home", "Cisco Webex Meetings - Home page is not opened");

        WebElement signInButton = webDriver.findElement(By.id("guest_signin_split_button-action"));
        Assert.assertTrue(signInButton.isDisplayed(), "SignIn button is not displayed");
        signInButton.click();
    }

    @Test
    public void loginWebex() {
        WebElement EmailInputField = webDriver.findElement(By.cssSelector("input#IDToken1"));
//        WebElement EmailInputField = webDriver.findElement(By.id("IDToken1"));
        EmailInputField.sendKeys("Tatiana.Kozlova@t-systems.com");

        WebElement signInButtonAfterEmailInput = webDriver.findElement(By.id("IDButton2"));
//        WebElement signInButtonAfterEmailInput = webDriver.findElement(By.name("btnOk"));

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(signInButtonAfterEmailInput.isDisplayed(), "Sign In button is not displayed");
        softAssert.assertEquals(signInButtonAfterEmailInput.getText(), "Sign In", "Message on this button is not Sign In");
        softAssert.assertAll();

        signInButtonAfterEmailInput.click();

        WebElement loginByMyPortalAccountAlternative = webDriver.findElement(By.xpath("//span[contains(text(), 'MyPortal Account (Alternative)')]"));
        loginByMyPortalAccountAlternative.click();

        WebElement CIAMUserIdInputField = webDriver.findElement(By.cssSelector("input#username.form-input"));
        CIAMUserIdInputField.sendKeys("tkozlova");

        WebElement CIAMPasswordInputField = webDriver.findElement(By.cssSelector("input#password.form-input"));
        CIAMPasswordInputField.sendKeys("Tanya202106*");

        WebElement loginButton = webDriver.findElement(By.cssSelector("input#submit.btn.btn-large.btn-block.btn-brand.offset-top-0"));
        loginButton.click();

        WebElement labelLoginCheck = webDriver.findElement(By.xpath("//h2[contains(text(), 'Tatiana Kozlova')]"));
        Assert.assertEquals(labelLoginCheck.getText(), "Persönlicher Raum von Tatiana Kozlova", "Login with incorrect user");
    }
}